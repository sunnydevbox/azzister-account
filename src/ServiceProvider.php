<?php

namespace Sunnydevbox\AzzisterAccount;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('tw-core.php'),
        ]);

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        //$this->loadMigrationsFrom(__DIR__.'/..//database/migrations');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/config.php', 'tw-core'
        );
    }

    /** OVERRIDE */
    public function mergeConfig()
    {
        return [
            __DIR__ . '/../config/config.php' => 'tw-core'
        ];
    }

    public function registerProviders()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        // if (config('tw-core.enable_log_viewer') === true && !$this->app->runningInConsole()) {

        // if (class_exists('Barryvdh\Cors\ServiceProvider')
        //     && !$this->app->resolved('Barryvdh\Cors\ServiceProvider')
        // ) {
        //     $this->app->register(\Barryvdh\Cors\ServiceProvider::class);
        // }

        // if (class_exists('\Dingo\Api\Provider\LaravelServiceProvider')
        //     && !$this->app->resolved('\Dingo\Api\Provider\LaravelServiceProvider')
        // ) {
        //     $this->app->register(\Dingo\Api\Provider\LaravelServiceProvider::class);
        // }

        // if (class_exists('\Prettus\Repository\Providers\RepositoryServiceProvider')
        //     && !$this->app->resolved('\Prettus\Repository\Providers\RepositoryServiceProvider')
        // ) {
        //     $this->app->register(\Prettus\Repository\Providers\RepositoryServiceProvider::class);
        // }

        // if (class_exists('\Tymon\JWTAuth\Providers\JWTAuthServiceProvider')
        //     && !$this->app->resolved('\Tymon\JWTAuth\Providers\JWTAuthServiceProvider')
        // ) {
        //     $this->app->register(\Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class);

        //     $loader->alias('JWTAuth', \Tymon\JWTAuth\Facades\JWTAuth::class);
        //     $loader->alias('JWTFactory', \Tymon\JWTAuth\Facades\JWTFactory::class);
        // }

        // if (class_exists('\Phoenix\EloquentMeta\ServiceProvider')
        //     && !$this->app->resolved('\Phoenix\EloquentMeta\ServiceProvider')
        // ) {
        //     $this->app->register(\Phoenix\EloquentMeta\ServiceProvider::class);
        // }

        // DEPRECATED
        // if (class_exists('\Spatie\Activitylog\ActivitylogServiceProvider')
        //     && !$this->app->resolved('\Spatie\Activitylog\ActivitylogServiceProvider')
        // ) {
        //     $this->app->register(\Spatie\Activitylog\ActivitylogServiceProvider::class);
        //     $loader->alias('Activity', Spatie\Activitylog\ActivitylogFacade::class);
        // }

        if (class_exists(\NZTim\Logger\LoggerServiceProvider::class)
            && !$this->app->resolved(\NZTim\Logger\LoggerServiceProvider::class)
        ) {
            $this->app->register(\NZTim\Logger\LoggerServiceProvider::class);
            $loader->alias('Logger', \NZTim\Logger\LoggerFacade::class);
        }

        if (class_exists(\Bnb\Laravel\Attachments\AttachmentsServiceProvider::class)
            && !$this->app->resolved(\Bnb\Laravel\Attachments\AttachmentsServiceProvider::class)
        ) {
            $this->app->register(\Bnb\Laravel\Attachments\AttachmentsServiceProvider::class);
        }
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\TWCore\Console\Commands\PublishMigrationsCommand::class,
                \Sunnydevbox\TWCore\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWCore\Console\Commands\OptimizeCommand::class,
                \Sunnydevbox\TWCore\Console\Commands\MigrateCommand::class,
            ]);

            /*$localViewFactory = $this->createLocalViewFactory();
            $this->app->singleton(
                'command.sleepingowl.ide.generate',
                function ($app) use ($localViewFactory) {
                    return new \SleepingOwl\Admin\Console\Commands\GeneratorCommand($app['config'], $app['files'], $localViewFactory);
                }
            ); 

            $this->commands('command.sleepingowl.ide.generate');*/
        }
    }
}
